/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from 'axios';

export function createPeer(isWebcam: boolean) {
    const peer = new RTCPeerConnection({
        iceServers: [
            {
                urls: 'stun:stun.stunprotocol.org',
            },
        ],
    });
    peer.onnegotiationneeded = () => handleNegotiationNeededEvent(peer, isWebcam);

    return peer;
}

async function handleNegotiationNeededEvent(peer: any, isWebcam: boolean) {
    const offer = await peer.createOffer();
    await peer.setLocalDescription(offer);
    const payload = {
        sdp: peer.localDescription,
    };
    const url = isWebcam ? `${import.meta.env.VITE_DEV_SERVER_URL}/broadcast` : `${import.meta.env.VITE_DEV_SERVER_URL}/broadcast`;
    console.log(url);
    const { data } = await axios.post(url, payload);
    const desc = new RTCSessionDescription(data.sdp);
    peer.setRemoteDescription(desc).catch((e: any) => console.log(e));
}