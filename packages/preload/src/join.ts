/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from 'axios';

export function createJoinPeer() {
    const peerStreamer = new RTCPeerConnection({
        iceServers: [
            {
                urls: 'stun:stun.stunprotocol.org',
            },
        ],
    });
    peerStreamer.ontrack = handleTrackStreamerEvent;
    peerStreamer.onnegotiationneeded = () => handleNegotiationNeededEvent(peerStreamer, true);
    return peerStreamer;
}

async function handleNegotiationNeededEvent(peer: any, isWebcam = false) {
    const offer = await peer.createOffer();
    await peer.setLocalDescription(offer);
    const payload = {
        sdp: peer.localDescription,
    };
    const url = isWebcam ? `${import.meta.env.VITE_DEV_SERVER_URL}/consumer` : `${import.meta.env.VITE_DEV_SERVER_URL}/consumer`;
    const { data } = await axios.post(url, payload);
    const desc = new RTCSessionDescription(data.sdp);
    peer.setRemoteDescription(desc).catch((e: any) => console.log(e));
}

function handleTrackStreamerEvent(e: any) {
    const video = document.getElementById('watch-stream') as HTMLMediaElement;
    video!.srcObject = e.streams[0];
    video!.onloadedmetadata = () => video!.play();
}